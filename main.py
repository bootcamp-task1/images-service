import boto3
import botocore
from botocore.client import Config
from flask import Flask, request
from flask_cors import CORS, cross_origin
from os import environ

app = Flask(__name__)
cors = CORS(app)

bucket_name = environ.get('S3_BUCKET', None)

if not all([environ.get('AWS_ACCESS_KEY_ID', None), environ.get('AWS_SECRET_ACCESS_KEY', None), bucket_name]):
    raise Exception('Missing necessary environment variables')

s3_client = boto3.client(
    service_name='s3',
    aws_access_key_id=environ.get('AWS_ACCESS_KEY_ID', None),
    aws_secret_access_key=environ.get('AWS_SECRET_ACCESS_KEY', None),
    region_name='eu-central-1',
    config=botocore.client.Config(signature_version='s3v4')
)

# Check if bucket is accessible
try:
    s3_client.head_bucket(Bucket=bucket_name)
except Exception as e:
    print(f"Error validating the S3 bucket: {e}")
    exit(1)

@app.route('/upload', methods=['POST'])
def upload_file():
    file = request.files['file']
    s3_client.upload_fileobj(file, bucket_name, file.filename)
    return "File uploaded successfully"

@app.route('/files', methods=['GET'])
def list_files():
    files = s3_client.list_objects(Bucket=bucket_name)

    response = []
    for file in files.get('Contents', []):
        presigned_url = s3_client.generate_presigned_url(
            'get_object',
            Params={'Bucket': bucket_name, 'Key': file['Key'], 'ResponseContentDisposition': 'inline'},
            ExpiresIn=3600,
        )
        response.append({'file': file['Key'], 'url': presigned_url})

    return {'files': response}

@app.route('/delete/<filename>', methods=['DELETE'])
def delete_file(filename):
    try:
        s3_client.delete_object(Bucket=bucket_name, Key=filename)
        return jsonify({'message': f'File {filename} deleted successfully'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5001)